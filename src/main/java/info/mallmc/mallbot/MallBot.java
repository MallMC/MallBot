package info.mallmc.mallbot;

import info.mallmc.core.MallCore;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.mallbot.util.Config;
import info.mallmc.mallbot.util.discord.DiscordBot;
import info.mallmc.mallbot.util.BotLogger;

import java.util.Scanner;

public class MallBot
{
    private static DiscordBot discordBot;
    private static MallCore mallCore;
    private static BotLogger botlogger;

    public static void main(String[] args) {
        botlogger = new BotLogger();
        botlogger.info("Starting Bot...");
        Config.readConfig();
        mallCore = new MallCore();
        mallCore.connect(Config.datebaseIP, Integer.parseInt(Config.datebasePort), Config.datebaseUsername, Config.datebasePassword);
        mallCore.redisConnect(Config.redisIP, Config.redisPassword, Integer.parseInt(Config.redisPort));
        discordBot = new DiscordBot(Config.botToken);
        mallCore.getDatabase().getMorphia().mapPackageFromClass(MallPlayer.class);
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            switch (scanner.next())
            {
                case "error":
                    botlogger.error("THIS IS A TEST MSG", "FAKE ERROR MSG");
                    break;
                case "info":
                    botlogger.info("THIS IS A TEST MSG");
                    break;
                case "debug":
                    botlogger.debug("FAKE ERROR MSG");
                    break;
            }
        }
    }


    public static BotLogger getBotLogger() {
        return botlogger;
    }

    public static DiscordBot getDiscordBot() {
        return discordBot;
    }
}
