package info.mallmc.mallbot.util;

import info.mallmc.mallbot.MallBot;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config
{
  public static String datebaseIP;
  public static String datebasePort;
  public static String datebaseUsername;
  public static String datebasePassword;
  public static String redisIP;
  public static String redisPassword;
  public static String redisPort;
  public static String botToken;

  public static void readConfig() {

    try {
      Properties props = new Properties();
      File file = new File("mallbot.config");
      if(!file.exists()) {
        file.createNewFile();
      }
      props.load(new FileInputStream(file));

      datebaseIP = props.getProperty("datebase.ip", "localhost");
      datebasePort = props.getProperty("datebase.port", "27019");
      datebaseUsername = props.getProperty("datebase.username", "mall");
      datebasePassword = props.getProperty("datebase.password");
      redisIP = props.getProperty("redis.ip", "localhost");
      redisPassword = props.getProperty("redis.password", "");
      redisPort = props.getProperty("redis.port", "6379");
      botToken = props.getProperty("discord.botToken");


    } catch (Exception e) {
      MallBot.getBotLogger().error(e.getMessage(), " ");
    }
  }

}
