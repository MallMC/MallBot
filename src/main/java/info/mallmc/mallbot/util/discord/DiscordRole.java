package info.mallmc.mallbot.util.discord;

import de.btobastian.javacord.entities.permissions.Role;
import info.mallmc.core.api.player.Rank;
import info.mallmc.mallbot.MallBot;

import java.util.ArrayList;
import java.util.List;

public enum DiscordRole
{
    BOTDEV("356496374470279168", -1),
    EVERYONE("", -1),
    CUSTOMER("343878707129352192", 0),
    LOCAL("343879084750798851", 1),
    LOYALTY_CUSTOMER("343879215323545600", 2),
    CONTRACTOR("236770068250230784", 3),
    STAFF("236769982757863427", 4),
    SECURITY("236769445488361475", 5),
    TESETER("343880482259337216", 6),
    DEVELOPER("236769195906433025", 7);

    private String roleID;
    private int rankId;

    DiscordRole(String id, int rankId)
    {
        this.roleID = id;
        this.rankId = rankId;
    }


    public Role getRole() {
        return MallBot.getDiscordBot().getServer().getRoleById(roleID);
    }

    public int getRankId() {
        return rankId;
    }

    public String getRoleID() {
        return roleID;
    }

    public static Role getRoleByRank(Rank rank) {
        Role role = null;
        for (DiscordRole discordRanks : DiscordRole.values()) {
            if (discordRanks.rankId == rank.getId()) {
                role = MallBot.getDiscordBot().getServer().getRoleById(discordRanks.getRoleID());
            }
        }
        return role;
    }

    public static List<String> getAllIds() {
        List<String> ids = new ArrayList<>();
        for (DiscordRole discordRole : DiscordRole.values()) {
            ids.add(discordRole.getRoleID());
        }
        return ids;
    }
}
