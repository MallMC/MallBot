package info.mallmc.mallbot.util.discord;

import com.google.common.util.concurrent.FutureCallback;
import de.btobastian.javacord.DiscordAPI;
import de.btobastian.javacord.Javacord;
import de.btobastian.javacord.entities.Server;
import de.btobastian.javacord.entities.User;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.mallbot.MallBot;
import info.mallmc.mallbot.events.core.EventMessageToDiscord;
import info.mallmc.mallbot.events.discord.DiscordChatEvent;

public class DiscordBot {

    private final DiscordAPI discordAPI;
    private Server server;

    public DiscordBot(String token) {
        discordAPI = Javacord.getApi(token, true);
        discordAPI.connect(new FutureCallback<DiscordAPI>() {
            @Override
            public void onSuccess(DiscordAPI result) {
                server = discordAPI.getServerById("236769160502181888");
                MallBot.getBotLogger().debug("Bot's server set to " + server.getName());
                discordAPI.registerListener(new DiscordChatEvent());
                EventMessageToDiscord.registerEvents();
            }

            @Override
            public void onFailure(Throwable t) {
                MallBot.getBotLogger().error("Failed to start bot", t.getMessage());
            }
        });
    }

    public DiscordAPI getDiscordAPI() {
        return discordAPI;
    }

    public Server getServer() {
        return server;
    }

    public void clearRanks(MallPlayer mallPlayer) throws Exception {
        User user = discordAPI.getUserById(mallPlayer.getDiscordID()).get();
        for (DiscordRole discordRole : DiscordRole.values()) {
            if(user.getRoles(server).contains(discordRole.getRole())) {
                discordRole.getRole().removeUser(user);
            }
        }
    }


    public void updateRank(MallPlayer mallPlayer) throws Exception {
        User user = discordAPI.getUserById(mallPlayer.getDiscordID()).get();
        DiscordRole.getRoleByRank(mallPlayer.getRank()).addUser(user);
    }
}
