package info.mallmc.mallbot.util.discord;

import de.btobastian.javacord.entities.Channel;
import info.mallmc.mallbot.MallBot;

public enum DiscordChannles
{
    STAFF("236771809972715523");

    private String id;

    DiscordChannles(String id)
    {
        this.id = id;
    }

    public String getId() {
      return id;
    }
    public Channel getChannle()
    {
      return MallBot.getDiscordBot().getDiscordAPI().getChannelById(id);
    }
}
