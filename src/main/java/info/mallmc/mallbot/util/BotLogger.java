package info.mallmc.mallbot.util;

import de.btobastian.javacord.entities.message.embed.EmbedBuilder;
import info.mallmc.mallbot.MallBot;
import info.mallmc.mallbot.util.discord.DiscordRole;

import java.awt.*;

public class BotLogger {

    public void info(Object logObject) {
        System.out.println("[Info] " + logObject.toString());
        if (MallBot.getDiscordBot() != null) {
            EmbedBuilder builder = new EmbedBuilder();
            builder.addField("Log Level", "Info", false);
            builder.setColor(Color.BLUE);
            builder.addField("Message", logObject.toString(), false);
            MallBot.getDiscordBot().getDiscordAPI().getChannelById("356493828955439105").sendMessage("", builder);

        }
    }

    public void debug(Object logObject) {
        System.out.println("[Debug] " + logObject.toString());
        if (MallBot.getDiscordBot() != null) {
            EmbedBuilder builder = new EmbedBuilder();
            builder.setColor(Color.ORANGE);
            builder.addField("Log Level", "Debug", false);
            builder.addField("Message", logObject.toString(), false);
            MallBot.getDiscordBot().getDiscordAPI().getChannelById("356493828955439105").sendMessage("", builder);
        }
    }

    public void error(String info, String error) {
        System.out.println("[Error] " + info);
        System.out.println("[Error Msg] " + error);
        if (MallBot.getDiscordBot() != null) {
            EmbedBuilder builder = new EmbedBuilder();
            builder.setColor(Color.RED);
            builder.addField("Log Level", "Error", false);
            builder.addField("Message", info, false);
            builder.addField("Error Message", error, false);
            MallBot.getDiscordBot().getDiscordAPI().getChannelById("356493828955439105").sendMessage(DiscordRole.BOTDEV.getRole().getMentionTag(), builder);
        }
    }
}

