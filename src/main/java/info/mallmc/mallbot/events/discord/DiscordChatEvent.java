package info.mallmc.mallbot.events.discord;

import de.btobastian.javacord.DiscordAPI;
import de.btobastian.javacord.entities.message.Message;
import de.btobastian.javacord.listener.Listener;
import de.btobastian.javacord.listener.message.MessageCreateListener;
import info.mallmc.core.api.player.Rank;
import info.mallmc.core.events.EventHandler;
import info.mallmc.core.events.EventType;
import info.mallmc.core.events.events.discord.DiscordMessageReceivedEvent;
import info.mallmc.mallbot.util.discord.DiscordChannles;

public class DiscordChatEvent implements MessageCreateListener {

  @Override
  public void onMessageCreate(DiscordAPI discordAPI, Message message) {
    if(message.getChannelReceiver().getId().equalsIgnoreCase(DiscordChannles.STAFF.getId()) && !(message.getAuthor().getId().equalsIgnoreCase("345261878341271552"))) {
      EventHandler.triggerEvent(EventType.MESSAGE_FORM_DISCORD_EVENT,  new DiscordMessageReceivedEvent(message.getContent(), message.getAuthor().getName(), Rank.DEFAULT))  ;
    }
  }
}