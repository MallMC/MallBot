package info.mallmc.mallbot.events.core;

import de.btobastian.javacord.entities.message.embed.EmbedBuilder;
import info.mallmc.core.events.Event;
import info.mallmc.core.events.EventHandler;
import info.mallmc.core.events.EventType;
import info.mallmc.core.events.events.discord.SendMessageToDiscordEvent;
import info.mallmc.core.util.discord.DiscordMessage;
import info.mallmc.core.util.discord.DiscordMessageType;
import info.mallmc.mallbot.util.discord.DiscordChannles;

public class EventMessageToDiscord
{

    public static void registerEvents() {
        EventHandler.registerEvent(EventType.MESSAGE_TO_DISCORD_EVENT, EventMessageToDiscord::onMessageIN);
    }

    private static void onMessageIN(Event event) {
        SendMessageToDiscordEvent discordLinkResultEvent = (SendMessageToDiscordEvent) event;
        DiscordMessage discordMessage = discordLinkResultEvent.getDiscordMessage();
        if(discordLinkResultEvent.getType() == DiscordMessageType.CHAT) {
            EmbedBuilder builder = new EmbedBuilder();
            builder.setTitle(discordMessage.getDescription());
            builder.setColor(discordMessage.getColor());
            builder.setAuthor(discordMessage.getTitle(), "", discordMessage.getAuthorIconUrl());
            DiscordChannles.STAFF.getChannle().sendMessage("", builder);
        }
        if(discordLinkResultEvent.getType() == DiscordMessageType.ALERT) {
            EmbedBuilder builder = new EmbedBuilder();
            builder.setTitle(discordMessage.getDescription());
            builder.setColor(discordMessage.getColor());
            builder.setAuthor(discordMessage.getTitle(), "", discordMessage.getAuthorIconUrl());
            DiscordChannles.STAFF.getChannle().sendMessage("", builder);
        }
    }
}
